package com.skyinyourcoffee.skytestmod.util;

public class Reference {
	public static final String MOD_ID = "skytestmod";
	public static final String NAME = "Sky Test Mod";
	public static final String VERSION = "0.01";
	public static final String ACCEPTED_VERSIONS = "[1.12.2]";
	public static final String CLIENT_PROXY_CLASS = "com.skyinyourcoffee.skytestmod.proxy.ClientProxy";
	public static final String COMMON_PROXY_CLASS = "com.skyinyourcoffee.skytestmod.proxy.CommonProxy";
}
