package com.skyinyourcoffee.skytestmod.init;

import java.util.ArrayList;
import java.util.List;

import com.skyinyourcoffee.skytestmod.blocks.BlockBase;
import com.skyinyourcoffee.skytestmod.blocks.BlockOres;
import com.skyinyourcoffee.skytestmod.blocks.RubyBlock;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class ModBlocks {
	
	public static final List<Block> BLOCKS = new ArrayList<Block>();
	
	public static final Block RUBY_BLOCK = new RubyBlock("ruby_block", Material.IRON);
	public static final Block DIRTY_BLOCK = new BlockBase("dirty_block", Material.GROUND);
	public static final Block ORE_OVERWORLD = new BlockOres("ore_overworld", "overworld");
}
