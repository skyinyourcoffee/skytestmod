package com.skyinyourcoffee.skytestmod.items.tools;

import com.skyinyourcoffee.skytestmod.Main;
import com.skyinyourcoffee.skytestmod.init.ModItems;
import com.skyinyourcoffee.skytestmod.util.IHasModel;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemSword;

public class ToolSword extends ItemSword implements IHasModel{
	public ToolSword(String name, ToolMaterial material) {
		super(material);
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(Main.TESTTAB);
		
		ModItems.ITEMS.add(this);
	}
	@Override
	public void registerModels() {
		Main.proxy.registerItemRenderer(this, 0, "inventory");
	}
}
