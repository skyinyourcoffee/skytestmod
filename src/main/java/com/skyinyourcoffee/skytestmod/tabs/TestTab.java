package com.skyinyourcoffee.skytestmod.tabs;

import com.skyinyourcoffee.skytestmod.init.ModItems;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class TestTab extends CreativeTabs{
	
	public TestTab(String label) {
		super(label);
		this.setBackgroundImageName("test.png");
	}

	@Override
	public ItemStack getTabIconItem() {
		return new ItemStack(ModItems.OBISIDIAN_INGOT);
	}

}
